const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost/test')

const Cat = mongoose.model('Cat', { name: String })

const kitty = new Cat({ name: 'Schrodinger' })
kitty.save().then(() => console.log('meow'))
