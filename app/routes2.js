const express = require('express')

const router = express.Router()
const routerCervezas = require('./routes/v2/cervezas.js')
const routerUsers = require('./routes/v2/users.js')

router.get('/', (req, res) => {
  res.json({ mensaje: '¡Bienvenido a nuestra Api con MongoDB ' })
})

// const Cerveza = require('./models/v2/Cerveza')
// router.get('/ambar', (req, res) => {
//   const miCerveza = new Cerveza({ nombre: 'Ambar' })
//   miCerveza.save((err, miCerveza) => {
//     if (err) return console.error(err)
//     console.log(`Guardada en bbdd ${miCerveza.nombre}`)
//   })
// })

router.use('/cervezas', routerCervezas)
// router.use('/products', routerProducts)
router.use('/users', routerUsers)

module.exports = router
