const express = require('express')
const router = express.Router()
const cervezaController = require('../controllers/cervezaController')

router.get('/', (req, res) => {
  console.log('vamos a pedir la lista por el enrutador')
  cervezaController.index(req, res)
})

router.get('/:id', (req, res) => {
  console.log('rutas de show cervezas')
  cervezaController.show(req, res)
  // res.json({ mensaje: `¡A beber ${req.params.id}!` })
})

router.post('/', (req, res) => {
  cervezaController.store(req, res)
  // res.json({ mensaje: `Cerveza creada: ${req.body.nombre}` })
})

router.delete('/:id', (req, res) => {
  cervezaController.destroy(req, res)
  // res.json({ mensaje: '¡Cerveza eliminada!' })
})

router.put('/:id', (req, res) => {
  cervezaController.update(req, res)
  // res.json({ mensaje: '¡Cervezas!' })
})

module.exports = router
