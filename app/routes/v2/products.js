const productController = requiere('../../controllers/v2/productController.js')
const servicejwt = require('../../services/servicejwt')
const auth = require('../../middlewares/auth')

router.use(auth)

router.use((req, res, next) => {
  console.log(req.headers.authorization)
  if (!req.headers.authorization) {
    return res.status(403).send({ message: 'No tienes permiso' })
  }
  const token = req.headers.authorization.split(' ')[1]
  // Authorization: Bearer XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
  try {
    payload = servicejwt.decodeToken(token)
  } catch (error) {
    return res.status(401).send(`${error}`)
  }
  next()
  // res.status(200).send({ message: 'con permiso' })
})
