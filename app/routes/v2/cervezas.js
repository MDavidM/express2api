const express = require('express')
const router = express.Router()
const cervezaController = require('../../controllers/v2/cervezaController.js')

router.get('/', (req, res) => {
  console.log('rutas de cervezas')
  // res.json({ mensaje: 'ok' })
  cervezaController.index(req, res)
})

router.get('/:id', (req, res) => {
  // res.json({ mensaje: 'ok' })
  cervezaController.show(req, res)
})

router.get('/search', (req, res) => {
  console.log('rutas de show cervezas')
  cervezaController.search(req, res)
  // res.json({ mensaje: `¡A beber ${req.params.id}!` })
})

router.post('/', (req, res) => {
  cervezaController.create(req, res)
  // res.json({ mensaje: `Cerveza creada: ${req.body.nombre}` })
})

router.delete('/:id', (req, res) => {
  cervezaController.remove(req, res)
  // res.json({ mensaje: '¡Cerveza eliminada!' })
})

router.put('/:id', (req, res) => {
  cervezaController.update(req, res)
  // res.json({ mensaje: '¡Cervezas!' })
})

module.exports = router
