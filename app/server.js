const express = require('express') // llamamos a Express
require('./config/db')
const app = express()
// estas dos primeras linieas las ponemos siempre, porque si, para ejecutar express
const port = process.env.PORT || 8080 // establecemos nuestro puerto

const bodyParser = require('body-parser')
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

const router = require('./routes.js')
const router2 = require('./routes2.js')

// nuestra ruta irá en http://localhost:8080/api
// es bueno que haya un prefijo, sobre todo por el tema de versiones de la API
app.use('/api', router)
app.use('/api2', router2)
// iniciamos nuestro servidor
app.listen(port, () => {
  console.log('API escuchando en el puerto ' + port)
})
